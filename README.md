# mkrepo

Make a Gitea repo with your own default settings.

## Config

**--username and --token are required**

> // On Unix systems, it returns $XDG_CONFIG_HOME as specified by  
// https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html if  
// non-empty, else $HOME/.config.  
// On Darwin, it returns $HOME/Library/Application Support.  
// On Windows, it returns %AppData%.  
// On Plan 9, it returns $home/lib.

* Above directory, file `.mkrepo.toml`
* Otherwise `$HOME/.mkrepo.toml`
* Otherwise `./.mkrepo.toml`

|Flag/TOML|Environment Variable|
|---|---|
|config|MKREPO_CONFIG|
|username|MKREPO_USERNAME|
|token|MKREPO_TOKEN|
|base-url|MKREPO_BASE-URL|
|template|MKREPO_TEMPLATE|
|private|MKREPO_PRIVATE|
|wiki|MKREPO_WIKI|
|issues|MKREPO_ISSUES|
|time-tracking|MKREPO_TIME_TRACKING|
|time-tracking-contributors-only|MKREPO_TIME_TRACKING_CONTRIBUTORS_ONLY|
|issue-dependencies|MKREPO_ISSUE_DEPENDENCIES|
|close-issues-non-default-branch|MKREPO_CLOSE_ISSUES_NON_DEFAULT_BRANCH|
|projects|MKREPO_PROJECTS|
|pull-requests|MKREPO_PULL_REQUESTS|
|ignore-whitespace|MKREPO_IGNORE_WHITESPACE|
|merge|MKREPO_MERGE|
|rebase-merge|MKREPO_REBASE_MERGE|
|rebase-no-ff-merge|MKREPO_REBASE_NO_FF_MERGE|
|squash-merge|MKREPO_SQUASH_MERGE|

## License

[MIT](LICENSE)