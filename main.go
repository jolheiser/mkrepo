package main

import (
	"os"

	"code.gitea.io/sdk/gitea"
	"github.com/peterbourgon/ff"
	"github.com/peterbourgon/ff/fftoml"
	"go.jolheiser.com/beaver"
)

func main() {
	fs := flags()
	if err := ff.Parse(fs, os.Args[1:],
		ff.WithEnvVarPrefix("MKREPO"),
		ff.WithConfigFileFlag("config"),
		ff.WithAllowMissingConfigFile(true),
		ff.WithConfigFileParser(fftoml.Parser)); err != nil {
		beaver.Error(err)
		return
	}
	if username == "" || token == "" {
		beaver.Error("--username and --token are required")
		return
	}
	if fs.NArg() < 1 {
		beaver.Error("Provide a repo name")
		return
	}
	repo := fs.Arg(0)

	client, err := gitea.NewClient(baseURL, gitea.SetToken(token))
	if err != nil {
		beaver.Error(err)
		return
	}

	if !update {
		if _, _, err = client.CreateRepo(gitea.CreateRepoOption{
			Name: repo,
		}); err != nil {
			beaver.Error(err)
			return
		}
	}

	if _, _, err := client.EditRepo(username, repo, gitea.EditRepoOption{
		Private:   &private,
		Template:  &template,
		HasIssues: &issues,
		InternalTracker: &gitea.InternalTracker{
			EnableTimeTracker:                timeTracking,
			AllowOnlyContributorsToTrackTime: timeTrackingContributorsOnly,
			EnableIssueDependencies:          issueDependencies,
		},
		HasWiki:                   &wiki,
		HasPullRequests:           &pullRequest,
		HasProjects:               &projects,
		IgnoreWhitespaceConflicts: &ignoreWhitespace,
		AllowMerge:                &normalMerge,
		AllowRebase:               &rebaseMerge,
		AllowRebaseMerge:          &rebaseNoFFMerge,
		AllowSquash:               &squashMerge,
	}); err != nil {
		beaver.Error(err)
		return
	}

	beaver.Infof("%s/%s/%s", baseURL, username, repo)
}
