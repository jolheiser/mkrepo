package main

import (
	"flag"
	"os"
	"path/filepath"
)

var (
	update                       bool
	config                       string
	username                     string
	token                        string
	baseURL                      string
	template                     bool
	private                      bool
	wiki                         bool
	issues                       bool
	timeTracking                 bool
	timeTrackingContributorsOnly bool
	issueDependencies            bool
	closeIssueNonDefaultBranch   bool
	projects                     bool
	pullRequest                  bool
	ignoreWhitespace             bool
	normalMerge                  bool
	rebaseMerge                  bool
	rebaseNoFFMerge              bool
	squashMerge                  bool
)

func flags() *flag.FlagSet {
	fs := flag.NewFlagSet("mkrepo", flag.ExitOnError)
	fs.BoolVar(&update, "update", false, "Update an existing repo")
	fs.StringVar(&config, "config", configPath(), "Path to config file")
	fs.StringVar(&username, "username", "", "Username")
	fs.StringVar(&token, "token", "", "Token")
	fs.StringVar(&baseURL, "base-url", "https://gitea.com", "Base Gitea URL")
	fs.BoolVar(&template, "template", false, "Repository is template")
	fs.BoolVar(&private, "private", false, "Repository is private")
	fs.BoolVar(&wiki, "wiki", true, "Wiki enabled")
	fs.BoolVar(&issues, "issues", true, "Issues enabled")
	fs.BoolVar(&timeTracking, "time-tracking", true, "Time tracking enabled")
	fs.BoolVar(&timeTrackingContributorsOnly, "time-tracking-contributors-only", true, "Time tracking enabled for contributors only")
	fs.BoolVar(&issueDependencies, "issue-dependencies", true, "Issue dependencies enabled")
	fs.BoolVar(&closeIssueNonDefaultBranch, "close-issues-non-default-branch", false, "Allow closing issues in commits made on a non-default branch")
	fs.BoolVar(&projects, "projects", true, "Projects (kanban) enabled")
	fs.BoolVar(&pullRequest, "pull-requests", true, "Pull Requests enabled")
	fs.BoolVar(&ignoreWhitespace, "ignore-whitespace", false, "Ignore whitespace for conflicts in PRs")
	fs.BoolVar(&normalMerge, "merge", true, "Plain merging enabled")
	fs.BoolVar(&rebaseMerge, "rebase-merge", true, "Rebase merging enabled")
	fs.BoolVar(&rebaseNoFFMerge, "rebase-no-ff-merge", true, "Rebase merging with explicit merge commits (--no-ff) enabled")
	fs.BoolVar(&squashMerge, "squash-merge", true, "Squash merging enabled")
	return fs
}

func configPath() string {
	var base string
	var err error
	base, err = os.UserConfigDir()
	if err != nil {
		base, err = os.UserHomeDir()
		if err != nil {
			base = "."
		}
	}
	return filepath.Join(base, ".mkrepo.toml")
}
