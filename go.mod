module go.jolheiser.com/mkrepo

go 1.16

require (
	code.gitea.io/sdk/gitea v0.14.0
	github.com/pelletier/go-toml v1.8.1 // indirect
	github.com/peterbourgon/ff v1.7.0
	go.jolheiser.com/beaver v1.1.1
)
